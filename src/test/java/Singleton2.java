package java;

/**
 * 懒汉模式
 * 缺点：多线程环境下无法保证单例效果，会多次执行 instance=new Singleton()，需要考虑到多线程
 */
public class Singleton2{

    private static Singleton2 instance;

    private Singleton2(){
    }

    public static Singleton2 getInstance(){
        if(instance==null){
            instance=new Singleton2();
        }
        return instance;
    }
}