package java;

/**
 * 同步方法或同步块 懒汉模式
 * 缺点：性能不高，同步范围太大，在实例化instacne后，获取实例仍然是同步的，效率太低，需要缩小同步的范围。
 */
public class Singleton3{

    private static Singleton3 instance;

    private Singleton3(){
    }

    public static synchronized Singleton3 getInstance(){
        if(instance==null){
            instance=new Singleton3();
        }
        return instance;
    }
}