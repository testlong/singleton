package java;

/**
 * 饿汉模式
 * 缺点：提前new出来实例了，并不是在第一次调用get方法时才实例化，没有进行延迟加载。
 */
public class Singleton1 {
    private static Singleton1 instance=new Singleton1();

    private Singleton1(){
    }

    public static Singleton1 getInstance(){
        return instance;
    }
}
