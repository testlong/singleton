package java;

/**
 * 枚举实现
 * java1.5才出现的枚举类
 * 这种方法在功能上与公有域方法相近，但是它更加简洁，无偿提供了序列化机制，绝对防止多次实例化，即使是在面对复杂序列化或者反射攻击的时候。虽然这种方法还没有广泛采用，但是单元素的枚举类型已经成为实现Singleton的最佳方法。 —-《Effective Java 中文版 第二版》
 */
public enum Singleton8 {
    INSTANCE;
}
