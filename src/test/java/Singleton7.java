package java;

/**
 * 静态内部类懒汉模式
 * 静态内部类在没有显示调用的时候是不会进行加载的，当执行了return InstanceHolder.instance后才加载初始化，这样就实现了正确的单例模式。
 */
public class Singleton7{

    private Singleton7(){
    }
    public static  Singleton7 getInstance(){
        return InstanceHolder.instance;
    }
    static class InstanceHolder{
        private static Singleton7 instance=new Singleton7();
    }
}