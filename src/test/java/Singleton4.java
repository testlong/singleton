package java;

/**
 * Created by laiguanglong on 2017/9/29.
 * 缺点：缩小同步范围，来提高性能，但是让然存在多次执行instance=new Singletom()的可能，由此引出double check
 */
public class Singleton4{

    private static Singleton4 instance;

    private Singleton4(){
    }

    public static  Singleton4 getInstance(){

        if(instance==null){
            synchronized(Singleton4.class){
                instance=new Singleton4();
            }
        }
        return instance;
    }
}